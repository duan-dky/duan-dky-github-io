---
title: Duan Dky
draft: false
role: CS Undergraduate
avatar: images/icon.png
bio: Be the best you can be.
organization:
  name: University
  url: https://duan-dky.me/
social:
  - icon: envelope
    iconPack: fas
    url: mailto:courage159357@gmail.com
  - icon: twitter
    iconPack: fab
    url: https://twitter.com/d_kaiyao
  - icon: github
    iconPack: fab
    url: https://github.com/duan-dky
  - icon: telegram
    iconPack: fab
    url: https://t.me/courage159357

weight: 1
widget:
  handler: about

  # Options: sm, md, lg and xl. Default is md.
  width:

  sidebar:
    # Options: left and right. Leave blank to hide.
    position:
    # Options: sm, md, lg and xl. Default is md.
    scale:
  
  background:
    # Options: primary, secondary, tertiary or any valid color value. Default is primary.
    color: secondary
    image:
    # Options: auto, cover and contain. Default is auto.
    size:
    # Options: center, top, right, bottom, left.
    position:
    # Options: fixed, local, scroll.
    attachment: 
---

## Self Introduction

I'm a CS Undergraduate in University, and I study linux and network security related knowledge.


