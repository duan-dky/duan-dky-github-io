---
title: Duan Dky
draft: false
role: 在校大学生
avatar: images/icon.png
bio: 做最好的自己
organization:
  name: 大学
  url: https://duan-dky.me/
social:
  - icon: envelope
    iconPack: fas
    url: mailto:postmaster@duan-dky.me
  - icon: twitter
    iconPack: fab
    url: https://twitter.com/d_kaiyao
  - icon: github
    iconPack: fab
    url: https://github.com/duan-dky
  - icon: telegram
    iconPack: fab
    url: https://t.me/courage159357

weight: 1
widget:
  handler: about

  # Options: sm, md, lg and xl. Default is md.
  width:

  sidebar:
    # Options: left and right. Leave blank to hide.
    position:
    # Options: sm, md, lg and xl. Default is md.
    scale:
  
  background:
    # Options: primary, secondary, tertiary or any valid color value. Default is primary.
    color: secondary
    image:
    # Options: auto, cover and contain. Default is auto.
    size:
    # Options: center, top, right, bottom, left.
    position:
    # Options: fixed, local, scroll.
    attachment: 
---

## 自我介绍

我是大学CS本科生，主要学习linux和网络安全相关知识。

