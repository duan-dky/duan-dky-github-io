---
title: 个人经历
draft: false
experiences:
  - title: 本科
    organization:
      name: STDU
      url: https://www.stdu.edu.cn
    dates: "2020 - 现今"
    location: 中国
    writeup: >
      我做过的项目：

      - [c-practice](https://github.com/duan-dky/c-practice)

      - [Java-matrix](https://github.com/duan-dky/Java-matrix)

      - [DataStructure](https://github.com/duan-dky/DataStructure)

      - [java-database](https://github.com/duan-dky/java-database)

      - [java-gradesystem](https://github.com/duan-dky/java-gradesystem)


weight: 3
widget:
  handler: experience

  # Options: sm, md, lg and xl. Default is md.
  width: lg

  sidebar:
    # Options: left and right. Leave blank to hide.
    position: left
    # Options: sm, md, lg and xl. Default is md.
    scale:

  background:
    # Options: primary, secondary, tertiary or any valid color value. Default is primary.
    color:
    image:
    # Options: auto, cover and contain. Default is auto.
    size:
    # Options: center, top, right, bottom, left.
    position:
    # Options: fixed, local, scroll.
    attachment:
---
