---
# Name
title: Duan Dky
role:
bio:
avatar:

organization:
  name:
  url:

# Check the available icons on https://fontawesome.com/.
# You can get similar results like this <i class="fab fa-github"></i> after searching.
# Then icon is github and iconPack is fab for this case.
social:
  - icon: envelope
    iconPack: fas
    url: mailto:courage159357@gmail.com
  - icon: twitter
    iconPack: fab
    url: https://twitter.com/d_kaiyao
  - icon: github
    iconPack: fab
    url: https://github.com/duan-dky
  - icon: telegram
    iconPack: fab
    url: https://t.me/courage159357
---